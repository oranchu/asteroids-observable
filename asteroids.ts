/**
 * Asteroids functionality:
 * The asteroids game loop is run using an observable function which extends the Observable class, called everyFirst.
 * this function takes two parameters:
 *    - An observable of type number
 *    - An array of observables of type KeyboardEvent
 * This function merges the observables passed in, and maintains the latest updates from each
 * observable when next is called. The rationale for creating this function is to allow observing an interval
 * which represents the game frame rate (60fps), along with two KeyboardEvents (hence the array of KeyboardEvent as a parameter),
 * which allow tracking emitted keydown and keyup events.
 * The inspiration for this function is the Rx.js merge function. This function could be generalised more to allow other events to
 * be observed, though for this use case it was simpler, and easier to read and understand the types of the returned observable by
 * just merging the three observables explicitly (interval, keydown, keyup).
 *
 * The interval portion of the observable allows timely updates to take place, which include:
 *    - Spawning new asteroids after a 2 second time delay
 *    - Updating each of the elements' position, using their set velocity
 * After encountering issues with implementing the keydown event stream, having difficulties with moving the player with multiple
 * keys simultaneously, instead of just listening to the keydown event and performing a thrust/rotation movement, both the keydown
 * and keyup events allow controlling the ship movement, which makes the gameplay more flexible and smoother to use. The idea was to
 * detect the keydown event for each of the movement directions (up, right, down & left), and move the ship in that direction
 * using its velocity until the keyup event is detected for the currently active movement state. As mentioned this allows keys to be
 * pressed and released, without affecting other movement keys that are pressed/released, by tracking the state of each movement key
 * through the stream
 *
 * The implemented features of the game include:
 * - Ship movement using thrust, with velocity applied to the ship and gradually decreased when the up/down key is released,
 *    providing deceleration
 * - Ship collision with an asteroid makes the player lose a life. the asteroids & bullets are destroyed on the collision to
 *    prevent multiple collisions occurring and potentially resulting in the player losing multiple lives from a single asteroid
 * - The player has 5 lives, which are displayed in a <p> element underneath the game canvas, and are updated when the player loses a life
 * - Track the player's score, and increase it when an asteroid is destroyed. The score is displayed above the game canvas
 * - Shoot bullets out of the ship, which collide with asteroids, destroying both the asteroid and the bullet, removing them from the
 *    game state & DOM
 * - Game balancing with the increased difficulty of the game throughout the game duration. Asteroids are more frequently spawned later on in the game
 * - Collisions with the boundary wraps the player, but destroys the bullets & asteroids. Having potentially thousands of objects added to
 *    the DOM and  having their attributes updated each frame was a considering I kept in mind. I developed this with performance in mind,
 *    which is why the GameObjects are removed from the game when outside the boundary and are unnecessary
 * - Styling improvements, adding fonts and colours to the viewport, as well as adjusting the fill colour of the player & asteroids
 *    when the player loses a life. The idea here is to gradually have the player/asteroids become darker until one life remains,
 *    which results in the objects only displaying an outline (black fill)
 * - Types are indicated everywhere, from function parameters and return types, to creating interfaces to represent the game state,
 *    commonly used objects like the x and y postion (with the Coords interface)
 * - functionality is separated into distinct functions that serve a specific purpose. Where possible pure functions have been created,
 *    which are indicated in the comments above the function
 *
 */

// Represents the (x, y) position/direction vector
interface Coords {
  x: number;
  y: number;
}

// Represents the game state which is updated and passed down the observer stream
interface GameState {
  gameActive: boolean;
  ship: GameObject | null;
  asteroids: GameObject[];
  bullets: GameObject[];
  points: number;
  lives: number;
  latestEvent: [number, KeyboardEvent, KeyboardEvent, number];
  movementState: {
    up: boolean;
    down: boolean;
    left: boolean;
    right: boolean;
  };
}

// Represents an object in the game, with a movement velocity
interface GameObject {
  elem: Elem;
  velocity: Coords;
}

// Spectrum of colours that represent the ship fill colour based on the player's lives remaining
const playerColourPalette = [
  '#2196F3',
  '#1976D2',
  '#1565C0',
  '#0D47A1',
  '#282823',
];

// Spectrum of colours that represent the asteroid fill colour based on the player's lives remaining
const asteroidColourPalette = [
  '#F44336',
  '#D32F2F',
  '#C62828',
  '#B71C1C',
  '#282823',
];

// Milliseconds interval to run game at 60fps
const fpsInterval = 17;

/**
 * A pure function that takes a rotation and returns a unit vector, for
 * getting the object's forward direction
 * @param rotation Rotation of the object in degrees, between 0 and 360
 */
function getForwardUnitVector(rotation: number): Coords {
  const radians = (rotation - 90) * (Math.PI / 180);

  return {
    x: Math.cos(radians),
    y: Math.sin(radians),
  };
}

/**
 * Takes a target and current position in the canvas and returns the unit vector of the direction
 * towards the target position
 *
 * This is a pure function that doesn't have any side effects
 *
 * @param targetPos The (x, y) position of the target which the object wants to move towards
 * @param currentPos The current (x, y) position of the object
 */
function getTargetUnitVector(targetPos: Coords, currentPos: Coords): Coords {
  // Direction vector
  const direction = {
    x: targetPos.x - currentPos.x,
    y: targetPos.y - currentPos.y,
  };

  const magnitude = Math.sqrt(
    Math.pow(direction.x, 2) + Math.pow(direction.y, 2),
  );

  // Unit vector
  return {
    x: direction.x / magnitude,
    y: direction.y / magnitude,
  };
}

/**
 *  Pure function that uniformly generates random x & y coordinates around the game canvas' boundary
 */
function getRandPosInsideBoundary(): Coords {
  // Left/right of boundary
  if (Math.random() < 0.5) {
    return {
      x: Math.random() < 0.5 ? 5 : 595, // Left or right x pos
      y: 600 * Math.random(), // y pos along canvas y axis
    };
  }
  return {
    x: 600 * Math.random(), // y pos along canvas x axis
    y: Math.random() < 0.5 ? 5 : 595, // Left or right y pos
  };
}

/**
 * Pure function which returns true if the circles are overlapping
 *
 * @param x1 x position of first circle
 * @param y1 y position of first circle
 * @param r1 radius of first circle
 * @param x2 x position of second circle
 * @param y2 y position of second circle
 * @param r2 radius of second circle
 */
function getIsCircleCollision(
  x1: number,
  y1: number,
  r1: number,
  x2: number,
  y2: number,
  r2: number,
): boolean {
  const dx = x1 - x2;
  const dy = y1 - y2;
  const distance = Math.sqrt(dx * dx + dy * dy);

  return distance < r1 + r2;
}

/**
 * Returns whether the coordinates provided are outside the canvas' boundary
 * This function is pure
 *
 * @param position x and y coordinates of element
 */
function getIsBoundaryCollision(position: Coords): boolean {
  if (position.x > 0 && position.x < 600) {
    return false;
  }
  if (position.y > 0 && position.y < 600) {
    return false;
  }
  return true;
}

/**
 * The custom everyFirst Observable class function is used to create a stream, then in the subscribe function
 * the game loop is run, resulting in impure updates to the HTML Elements in the DOM which are the GameObjects
 */
function asteroids() {
  const svg: HTMLElement = document.getElementById('canvas')!;
  const score: HTMLElement = document.getElementById('score')!;
  const livesElem = document.querySelector('#lives');

  // Initialise game state
  const gameState: GameState = {
    gameActive: true,
    ship: null,
    asteroids: [],
    bullets: [],
    points: 0,
    lives: 5,
    latestEvent: [0, KeyboardEvent.prototype, KeyboardEvent.prototype, -1],
    movementState: {
      up: false,
      down: false,
      left: false,
      right: false,
    },
  };

  const oKeydown = Observable.fromEvent<KeyboardEvent>(document, 'keydown');
  const oKeyup = Observable.fromEvent<KeyboardEvent>(document, 'keyup');
  const oframeRate = Observable.interval(fpsInterval);
  Observable.everyFirst(oframeRate, [oKeydown, oKeyup])
    // Keep track of the last & current interval/event, to check what has changed in this observer.next call
    // .scan(
    //   [],
    //   (
    //     stream: [number, KeyboardEvent, KeyboardEvent, number][],
    //     eventInterval: [number, KeyboardEvent, KeyboardEvent, number],
    //   ): [number, KeyboardEvent, KeyboardEvent, number][] =>
    //     [stream[stream.length - 1]].concat([eventInterval]),
    // )
    // Update the latest interval event and pass game state down the stream. Ensures game state variable decalared outside this observable is only mutated here
    .map(latest => {
      gameState.latestEvent = latest;
      return gameState;
    })
    // Handle asteroid/bullet instantiation & movement, along with ship movement, and collision between all objects
    .subscribe((state: GameState) => {
      if (state.gameActive) {
        if (state.ship === null) {
          // make a group for the spaceship and a transform to move it and rotate it
          // to animate the spaceship you will update the transform property
          state.ship = generateShip(svg);
        }

        if (state.latestEvent[3] === 0) {
          // Generate asteroid after delay
          if (state.latestEvent[0] % 3995 === 0) {
            // Increase likelihood of multiple asteroids being generated as the game duration increases
            // Every 10 seconds an additional possible asteroid spawn is added to the pool
            const spawnCount = Math.min(
              10,
              (Math.random() * state.latestEvent[0]) / 9996,
            );
            for (let i = 0; i < spawnCount; i++) {
              state.asteroids.push(
                generateAsteroid(svg, state.ship.elem, state.lives),
              );
            }
          }

          // Update all GameObjects' position
          handleShipMovement(state);

          // Detect collision and destroy GameObjects accordingly,
          handlePlayerBoundaryCollisions(state.ship);

          updateGameObjectsPosition(state, svg);

          handleObjectCollisions(state, svg);

          score.innerHTML = state.points.toString();
        }
        // Handle keyboard events for player movement and instantiating bullets
        else if (state.latestEvent[3] !== -1) {
          // Last observed Keyboard Event
          const lastKeyEvent =
            state.latestEvent[3] === 1
              ? state.latestEvent[1]
              : state.latestEvent[2];

          handleKeyboardEvents(state, lastKeyEvent, svg);
        }

        return state;
      }
      // Display the game over message
      if (livesElem) {
        livesElem.innerHTML = 'GAME OVER!';
      }
    });
}

/**
 * Creates and returns a new ship GameObject. The game state is not changed in this function,
 * Although a new HTML Element will be created and added to the DOM, which is a side effect
 * @param svg The HTML Element that is the parent of the created ship Elem
 */
function generateShip(svg: HTMLElement): GameObject {
  const initialRot = Math.random() * 359;
  const ship = new Elem(svg, 'g')
    .attr('transform', 'translate(300 300) rotate(' + initialRot + ')')
    .attr('x', 300)
    .attr('y', 300)
    .attr('rot', initialRot);

  // create a polygon shape for the space ship as a child of the transform group
  new Elem(svg, 'polygon', ship.elem)
    .attr('points', '-10,15 10,15 0,-20')
    .attr(
      'style',
      `fill: ${playerColourPalette[0]}; stroke: #1976D2; stroke-width: 2`,
    );

  return {
    elem: ship,
    velocity: {
      x: 0,
      y: 0,
    },
  };
}

/**
 * Creates and returns a new bullet GameObject. The game state is not changed in this function,
 * Although a new HTML Element will be created and added to the DOM, which is a side effect
 * @param svg The HTML Element that is the parent of the created bullet Elem
 */
function generateBullet(svg: HTMLElement, ship: Elem): GameObject {
  const direction = getForwardUnitVector(Number(ship.attr('rot')));
  const elem = new Elem(svg, 'ellipse')
    .attr('cx', Number(ship.attr('x')) + direction.x * 20)
    .attr('cy', Number(ship.attr('y')) + direction.y * 20)
    .attr('rx', 3)
    .attr('ry', 3)
    .attr('fill', '#FFFFFF');
  const velocity = {
    x: direction.x * 10,
    y: direction.y * 10,
  };

  return {
    elem,
    velocity,
  };
}

/**
 * Creates and returns a new asteroid GameObject. The game state is not changed in this function,
 * Although a new HTML Element will be created and added to the DOM, which is a side effect
 * @param svg The HTML Element that is the parent of the created asteroid Elem
 */
function generateAsteroid(
  svg: HTMLElement,
  ship: Elem,
  livesRemaining: number,
): GameObject {
  const randomPos = getRandPosInsideBoundary();
  const playerPos = {
    x: Number(ship.attr('x')),
    y: Number(ship.attr('y')),
  };
  // Asteroid moves towards the player at the time of spawning
  const targetDirection = getTargetUnitVector(playerPos, randomPos);

  const elem = new Elem(svg, 'ellipse')
    .attr('cx', randomPos.x)
    .attr('cy', randomPos.y)
    .attr('rx', 20)
    .attr('ry', 20)
    .attr(
      'style',
      `fill: ${
        asteroidColourPalette[5 - livesRemaining]
      }; stroke: #D32F2F; stroke-width: 4`,
    );

  const velocity = {
    x: targetDirection.x * 1.2,
    y: targetDirection.y * 1.2,
  };

  return {
    elem,
    velocity,
  };
}

/**
 * This function has side effects. The ship parameter is of type GameObject, which contains an attribute of type Elem.
 * The element contains attributes which represent the X and Y position of the element, as well as the rotation, which
 * are all changed, along with the transform attribute, which is why this function is impure
 *
 * The game state is passed as a parameter, which is then updated and returns the updated state
 */
function handleShipMovement(state: GameState) {
  if (state.ship) {
    const direction = getForwardUnitVector(Number(state.ship.elem.attr('rot')));
    if (state.movementState.up) {
      state.ship.velocity = {
        x: direction.x * 4,
        y: direction.y * 4,
      };
    }
    if (state.movementState.down) {
      state.ship.velocity = {
        x: direction.x * -2.5,
        y: direction.y * -2.5,
      };
    }

    // Reduce velocity of ship if the up & down key are both not held
    if (!state.movementState.up && !state.movementState.down) {
      state.ship.velocity = {
        x: state.ship.velocity.x * 0.95,
        y: state.ship.velocity.y * 0.95,
      };
    }

    let rotation = Number(state.ship.elem.attr('rot'));
    if (state.movementState.right) {
      rotation += 3.5;
    }
    if (state.movementState.left) {
      rotation -= 3.5;
    }

    state.ship.elem
      .attr('x', Number(state.ship.elem.attr('x')) + state.ship.velocity.x)
      .attr('y', Number(state.ship.elem.attr('y')) + state.ship.velocity.y);

    state.ship.elem.attr('rot', rotation);

    state.ship.elem.attr(
      'transform',
      'translate(' +
        state.ship.elem.attr('x') +
        ' ' +
        state.ship.elem.attr('y') +
        ') rotate(' +
        rotation +
        ')',
    );
  }
  return state;
}

/**
 * Impure function which updates the Elem attributes of the asteroids and bullets using their velocity
 */
function updateGameObjectsPosition(state: GameState, svg: HTMLElement) {
  let asteroidRemoveIndices: number[] = [];
  let bulletRemoveIndices: number[] = [];

  state.asteroids.forEach((asteroid: GameObject, asteroidIndex: number) => {
    asteroid.elem
      .attr('cx', Number(asteroid.elem.attr('cx')) + asteroid.velocity.x)
      .attr('cy', Number(asteroid.elem.attr('cy')) + asteroid.velocity.y);

    // Destroy asteroids that have collided with boundary
    if (
      getIsBoundaryCollision({
        x: Number(asteroid.elem.attr('cx')),
        y: Number(asteroid.elem.attr('cy')),
      })
    ) {
      asteroidRemoveIndices.push(asteroidIndex);
    }
  });

  state.bullets.forEach((bullet, bulletIndex) => {
    if (bullet.velocity.x !== 0 || bullet.velocity.y !== 0) {
      bullet.elem.attr(
        'cx',
        Number(bullet.elem.attr('cx')) + bullet.velocity.x,
      );
      bullet.elem.attr(
        'cy',
        Number(bullet.elem.attr('cy')) + bullet.velocity.y,
      );

      // Destroy bullets that have collided with boundary
      if (
        getIsBoundaryCollision({
          x: Number(bullet.elem.attr('cx')),
          y: Number(bullet.elem.attr('cy')),
        })
      ) {
        bulletRemoveIndices.push(bulletIndex);
      }
    }
  });

  // Remove asteroids
  for (let i = asteroidRemoveIndices.length - 1; i >= 0; i--) {
    svg.removeChild(state.asteroids[i].elem.elem);
    state.asteroids.splice(i, 1);
  }

  // Remove bullets
  for (let i = bulletRemoveIndices.length - 1; i >= 0; i--) {
    svg.removeChild(state.bullets[i].elem.elem);
    state.bullets.splice(i, 1);
  }
}

/**
 * Impure function which takes the last KeyboardEvent as input and updates the game state to handle movement,
 * or generates bullets if space is pressed. This will alter the state, which was declared and instantiated
 * outside of this function's scope by pushing to the bullets array and updating the 'movementState'
 *
 * @param state
 * @param lastKeyEvent
 * @param svg
 */
function handleKeyboardEvents(
  state: GameState,
  lastKeyEvent: KeyboardEvent,
  svg: HTMLElement,
): void {
  if (state.ship) {
    if (lastKeyEvent.type === 'keydown') {
      switch (lastKeyEvent.code) {
        case 'Space':
          state.bullets.push(generateBullet(svg, state.ship.elem));
          break;
        case 'ArrowUp':
          state.movementState.up = true;
          break;
        case 'ArrowDown':
          state.movementState.down = true;
          break;
        case 'ArrowRight':
          state.movementState.right = true;
          break;
        case 'ArrowLeft':
          state.movementState.left = true;
          break;
      }
    } else if (lastKeyEvent.type === 'keyup') {
      switch (lastKeyEvent.code) {
        case 'ArrowUp':
          state.movementState.up = false;
          break;
        case 'ArrowDown':
          state.movementState.down = false;
          break;
        case 'ArrowRight':
          state.movementState.right = false;
          break;
        case 'ArrowLeft':
          state.movementState.left = false;
          break;
      }
    }
  }
}

/**
 * Impure function which updates the ship's x & y Elem attributes to wrap the ship
 *
 * @param ship GameObject representing the ship, containing velocity and the HTML Element
 */
function handlePlayerBoundaryCollisions(ship: GameObject) {
  const xPos = Number(ship.elem.attr('x'));
  const yPos = Number(ship.elem.attr('y'));
  if (xPos > 600) {
    ship.elem.attr('x', 0);
  } else if (xPos < 0) {
    ship.elem.attr('x', 600);
  }
  if (yPos > 600) {
    ship.elem.attr('y', 0);
  } else if (yPos < 0) {
    ship.elem.attr('y', 600);
  }
}

/**
 * Handle Player <---> asteroid & bullet ---> asteroid collisions
 * Impure function as it updates the game state, changing the asteroids & bullets array, as well as removing
 * their DOM elements.
 * Updates the player's lives, as well as the visual representation of the lives remaining by changing the text of the
 * <p> element
 */
function handleObjectCollisions(state: GameState, svg: HTMLElement) {
  let asteroidRemoveIndices: number[] = [];
  let bulletRemoveIndices: number[] = [];

  // Player -> Asteroids collision
  state.asteroids.forEach((asteroid: GameObject, asteroidIndex: number) => {
    if (state.ship !== null) {
      // Simplify player collision, create a small circle inside the ship triangle, which acts as the
      // bounding circle to be used for collision
      if (
        getIsCircleCollision(
          Number(state.ship.elem.attr('x')),
          Number(state.ship.elem.attr('y')),
          5, // Radius of 5 for the ship
          Number(asteroid.elem.attr('cx')),
          Number(asteroid.elem.attr('cy')),
          Number(asteroid.elem.attr('rx')),
        )
      ) {
        // Player loses a life
        state.lives -= 1;
        // Update the DOM element which displays the number of lives remaining
        const livesElem: HTMLElement | null = document.querySelector('#lives');
        if (livesElem) {
          if (state.lives > 0) {
            // Generate array of n hearts, reduce to string with spaces between
            livesElem.innerHTML = [...Array(state.lives).keys()]
              .map(_ => '<3')
              .reduce((p, c) => (p += ' ' + c));
            // Reset gameobjects after life is lost
            resetGameObjects(state, svg);
          } else {
            // Handle game over
            state.gameActive = false;
          }
        }
      }
    }

    // Asteroid -> bullet collision
    state.bullets.forEach((bullet: GameObject, bulletIndex: number) => {
      if (
        getIsCircleCollision(
          Number(asteroid.elem.attr('cx')),
          Number(asteroid.elem.attr('cy')),
          Number(asteroid.elem.attr('rx')),
          Number(bullet.elem.attr('cx')),
          Number(bullet.elem.attr('cy')),
          Number(bullet.elem.attr('rx')),
        )
      ) {
        // Destroy bullet - Mutates bullets array - Impure
        bulletRemoveIndices.push(bulletIndex);

        // Destroy/break up asteroid
        asteroidRemoveIndices.push(asteroidIndex);
        // Increase game score
        state.points += 100;
      }
    });
  });

  // Remove asteroids
  for (let i = asteroidRemoveIndices.length - 1; i >= 0; i--) {
    state.asteroids[asteroidRemoveIndices[i]].elem.elem.remove();
    state.asteroids.splice(asteroidRemoveIndices[i], 1);
  }

  // Remove bullets
  for (let i = bulletRemoveIndices.length - 1; i >= 0; i--) {
    if (
      state.bullets[bulletRemoveIndices[i]] &&
      state.bullets[bulletRemoveIndices[i]].elem
    ) {
      state.bullets[bulletRemoveIndices[i]].elem.elem.remove();
      state.bullets.splice(bulletRemoveIndices[i], 1);
    }
  }
}

/**
 * Resets the game state by removing all the asteroids and bullets from the game.
 * Also updates styling of elements in the viewport
 *
 * Impure function as it updates the game state and styling of ship element
 */
function resetGameObjects(state: GameState, svg: HTMLElement) {
  state.asteroids.forEach((asteroid: GameObject) => {
    svg.removeChild(asteroid.elem.elem);
  });
  state.bullets.forEach((bullet: GameObject) => {
    svg.removeChild(bullet.elem.elem);
  });
  state.asteroids = [];
  state.bullets = [];

  if (state.ship) {
    const shipPolygon = state.ship.elem.elem.firstElementChild;
    if (shipPolygon) {
      shipPolygon.setAttribute(
        'style',
        `fill: ${
          playerColourPalette[5 - state.lives]
        }; stroke: #1976D2; stroke-width: 2)`,
      );
    }
  }
}

// the following simply runs your asteroids function on window load.  Make sure to leave it in place.
if (typeof window != 'undefined')
  window.onload = () => {
    asteroids();
  };
