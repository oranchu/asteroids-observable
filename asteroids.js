"use strict";
const playerColourPalette = [
    '#2196F3',
    '#1976D2',
    '#1565C0',
    '#0D47A1',
    '#282823',
];
const asteroidColourPalette = [
    '#F44336',
    '#D32F2F',
    '#C62828',
    '#B71C1C',
    '#282823',
];
const fpsInterval = 17;
function getForwardUnitVector(rotation) {
    const radians = (rotation - 90) * (Math.PI / 180);
    return {
        x: Math.cos(radians),
        y: Math.sin(radians),
    };
}
function getTargetUnitVector(targetPos, currentPos) {
    const direction = {
        x: targetPos.x - currentPos.x,
        y: targetPos.y - currentPos.y,
    };
    const magnitude = Math.sqrt(Math.pow(direction.x, 2) + Math.pow(direction.y, 2));
    return {
        x: direction.x / magnitude,
        y: direction.y / magnitude,
    };
}
function getRandPosInsideBoundary() {
    if (Math.random() < 0.5) {
        return {
            x: Math.random() < 0.5 ? 5 : 595,
            y: 600 * Math.random(),
        };
    }
    return {
        x: 600 * Math.random(),
        y: Math.random() < 0.5 ? 5 : 595,
    };
}
function getIsCircleCollision(x1, y1, r1, x2, y2, r2) {
    const dx = x1 - x2;
    const dy = y1 - y2;
    const distance = Math.sqrt(dx * dx + dy * dy);
    return distance < r1 + r2;
}
function getIsBoundaryCollision(position) {
    if (position.x > 0 && position.x < 600) {
        return false;
    }
    if (position.y > 0 && position.y < 600) {
        return false;
    }
    return true;
}
function asteroids() {
    const svg = document.getElementById('canvas');
    const score = document.getElementById('score');
    const livesElem = document.querySelector('#lives');
    const gameState = {
        gameActive: true,
        ship: null,
        asteroids: [],
        bullets: [],
        points: 0,
        lives: 5,
        latestEvent: [0, KeyboardEvent.prototype, KeyboardEvent.prototype, -1],
        movementState: {
            up: false,
            down: false,
            left: false,
            right: false,
        },
    };
    const oKeydown = Observable.fromEvent(document, 'keydown');
    const oKeyup = Observable.fromEvent(document, 'keyup');
    const oframeRate = Observable.interval(fpsInterval);
    Observable.everyFirst(oframeRate, [oKeydown, oKeyup])
        .map(latest => {
        gameState.latestEvent = latest;
        return gameState;
    })
        .subscribe((state) => {
        if (state.gameActive) {
            if (state.ship === null) {
                state.ship = generateShip(svg);
            }
            if (state.latestEvent[3] === 0) {
                if (state.latestEvent[0] % 3995 === 0) {
                    const spawnCount = Math.min(10, (Math.random() * state.latestEvent[0]) / 9996);
                    for (let i = 0; i < spawnCount; i++) {
                        state.asteroids.push(generateAsteroid(svg, state.ship.elem, state.lives));
                    }
                }
                handleShipMovement(state);
                handlePlayerBoundaryCollisions(state.ship);
                updateGameObjectsPosition(state, svg);
                handleObjectCollisions(state, svg);
                score.innerHTML = state.points.toString();
            }
            else if (state.latestEvent[3] !== -1) {
                const lastKeyEvent = state.latestEvent[3] === 1
                    ? state.latestEvent[1]
                    : state.latestEvent[2];
                handleKeyboardEvents(state, lastKeyEvent, svg);
            }
            return state;
        }
        if (livesElem) {
            livesElem.innerHTML = 'GAME OVER!';
        }
    });
}
function generateShip(svg) {
    const initialRot = Math.random() * 359;
    const ship = new Elem(svg, 'g')
        .attr('transform', 'translate(300 300) rotate(' + initialRot + ')')
        .attr('x', 300)
        .attr('y', 300)
        .attr('rot', initialRot);
    new Elem(svg, 'polygon', ship.elem)
        .attr('points', '-10,15 10,15 0,-20')
        .attr('style', `fill: ${playerColourPalette[0]}; stroke: #1976D2; stroke-width: 2`);
    return {
        elem: ship,
        velocity: {
            x: 0,
            y: 0,
        },
    };
}
function generateBullet(svg, ship) {
    const direction = getForwardUnitVector(Number(ship.attr('rot')));
    const elem = new Elem(svg, 'ellipse')
        .attr('cx', Number(ship.attr('x')) + direction.x * 20)
        .attr('cy', Number(ship.attr('y')) + direction.y * 20)
        .attr('rx', 3)
        .attr('ry', 3)
        .attr('fill', '#FFFFFF');
    const velocity = {
        x: direction.x * 10,
        y: direction.y * 10,
    };
    return {
        elem,
        velocity,
    };
}
function generateAsteroid(svg, ship, livesRemaining) {
    const randomPos = getRandPosInsideBoundary();
    const playerPos = {
        x: Number(ship.attr('x')),
        y: Number(ship.attr('y')),
    };
    const targetDirection = getTargetUnitVector(playerPos, randomPos);
    const elem = new Elem(svg, 'ellipse')
        .attr('cx', randomPos.x)
        .attr('cy', randomPos.y)
        .attr('rx', 20)
        .attr('ry', 20)
        .attr('style', `fill: ${asteroidColourPalette[5 - livesRemaining]}; stroke: #D32F2F; stroke-width: 4`);
    const velocity = {
        x: targetDirection.x * 1.2,
        y: targetDirection.y * 1.2,
    };
    return {
        elem,
        velocity,
    };
}
function handleShipMovement(state) {
    if (state.ship) {
        const direction = getForwardUnitVector(Number(state.ship.elem.attr('rot')));
        if (state.movementState.up) {
            state.ship.velocity = {
                x: direction.x * 4,
                y: direction.y * 4,
            };
        }
        if (state.movementState.down) {
            state.ship.velocity = {
                x: direction.x * -2.5,
                y: direction.y * -2.5,
            };
        }
        if (!state.movementState.up && !state.movementState.down) {
            state.ship.velocity = {
                x: state.ship.velocity.x * 0.95,
                y: state.ship.velocity.y * 0.95,
            };
        }
        let rotation = Number(state.ship.elem.attr('rot'));
        if (state.movementState.right) {
            rotation += 3.5;
        }
        if (state.movementState.left) {
            rotation -= 3.5;
        }
        state.ship.elem
            .attr('x', Number(state.ship.elem.attr('x')) + state.ship.velocity.x)
            .attr('y', Number(state.ship.elem.attr('y')) + state.ship.velocity.y);
        state.ship.elem.attr('rot', rotation);
        state.ship.elem.attr('transform', 'translate(' +
            state.ship.elem.attr('x') +
            ' ' +
            state.ship.elem.attr('y') +
            ') rotate(' +
            rotation +
            ')');
    }
    return state;
}
function updateGameObjectsPosition(state, svg) {
    let asteroidRemoveIndices = [];
    let bulletRemoveIndices = [];
    state.asteroids.forEach((asteroid, asteroidIndex) => {
        asteroid.elem
            .attr('cx', Number(asteroid.elem.attr('cx')) + asteroid.velocity.x)
            .attr('cy', Number(asteroid.elem.attr('cy')) + asteroid.velocity.y);
        if (getIsBoundaryCollision({
            x: Number(asteroid.elem.attr('cx')),
            y: Number(asteroid.elem.attr('cy')),
        })) {
            asteroidRemoveIndices.push(asteroidIndex);
        }
    });
    state.bullets.forEach((bullet, bulletIndex) => {
        if (bullet.velocity.x !== 0 || bullet.velocity.y !== 0) {
            bullet.elem.attr('cx', Number(bullet.elem.attr('cx')) + bullet.velocity.x);
            bullet.elem.attr('cy', Number(bullet.elem.attr('cy')) + bullet.velocity.y);
            if (getIsBoundaryCollision({
                x: Number(bullet.elem.attr('cx')),
                y: Number(bullet.elem.attr('cy')),
            })) {
                bulletRemoveIndices.push(bulletIndex);
            }
        }
    });
    for (let i = asteroidRemoveIndices.length - 1; i >= 0; i--) {
        svg.removeChild(state.asteroids[i].elem.elem);
        state.asteroids.splice(i, 1);
    }
    for (let i = bulletRemoveIndices.length - 1; i >= 0; i--) {
        svg.removeChild(state.bullets[i].elem.elem);
        state.bullets.splice(i, 1);
    }
}
function handleKeyboardEvents(state, lastKeyEvent, svg) {
    if (state.ship) {
        if (lastKeyEvent.type === 'keydown') {
            switch (lastKeyEvent.code) {
                case 'Space':
                    state.bullets.push(generateBullet(svg, state.ship.elem));
                    break;
                case 'ArrowUp':
                    state.movementState.up = true;
                    break;
                case 'ArrowDown':
                    state.movementState.down = true;
                    break;
                case 'ArrowRight':
                    state.movementState.right = true;
                    break;
                case 'ArrowLeft':
                    state.movementState.left = true;
                    break;
            }
        }
        else if (lastKeyEvent.type === 'keyup') {
            switch (lastKeyEvent.code) {
                case 'ArrowUp':
                    state.movementState.up = false;
                    break;
                case 'ArrowDown':
                    state.movementState.down = false;
                    break;
                case 'ArrowRight':
                    state.movementState.right = false;
                    break;
                case 'ArrowLeft':
                    state.movementState.left = false;
                    break;
            }
        }
    }
}
function handlePlayerBoundaryCollisions(ship) {
    const xPos = Number(ship.elem.attr('x'));
    const yPos = Number(ship.elem.attr('y'));
    if (xPos > 600) {
        ship.elem.attr('x', 0);
    }
    else if (xPos < 0) {
        ship.elem.attr('x', 600);
    }
    if (yPos > 600) {
        ship.elem.attr('y', 0);
    }
    else if (yPos < 0) {
        ship.elem.attr('y', 600);
    }
}
function handleObjectCollisions(state, svg) {
    let asteroidRemoveIndices = [];
    let bulletRemoveIndices = [];
    state.asteroids.forEach((asteroid, asteroidIndex) => {
        if (state.ship !== null) {
            if (getIsCircleCollision(Number(state.ship.elem.attr('x')), Number(state.ship.elem.attr('y')), 5, Number(asteroid.elem.attr('cx')), Number(asteroid.elem.attr('cy')), Number(asteroid.elem.attr('rx')))) {
                state.lives -= 1;
                const livesElem = document.querySelector('#lives');
                if (livesElem) {
                    if (state.lives > 0) {
                        livesElem.innerHTML = [...Array(state.lives).keys()]
                            .map(_ => '<3')
                            .reduce((p, c) => (p += ' ' + c));
                        resetGameObjects(state, svg);
                    }
                    else {
                        state.gameActive = false;
                    }
                }
            }
        }
        state.bullets.forEach((bullet, bulletIndex) => {
            if (getIsCircleCollision(Number(asteroid.elem.attr('cx')), Number(asteroid.elem.attr('cy')), Number(asteroid.elem.attr('rx')), Number(bullet.elem.attr('cx')), Number(bullet.elem.attr('cy')), Number(bullet.elem.attr('rx')))) {
                bulletRemoveIndices.push(bulletIndex);
                asteroidRemoveIndices.push(asteroidIndex);
                state.points += 100;
            }
        });
    });
    for (let i = asteroidRemoveIndices.length - 1; i >= 0; i--) {
        state.asteroids[asteroidRemoveIndices[i]].elem.elem.remove();
        state.asteroids.splice(asteroidRemoveIndices[i], 1);
    }
    for (let i = bulletRemoveIndices.length - 1; i >= 0; i--) {
        if (state.bullets[bulletRemoveIndices[i]] &&
            state.bullets[bulletRemoveIndices[i]].elem) {
            state.bullets[bulletRemoveIndices[i]].elem.elem.remove();
            state.bullets.splice(bulletRemoveIndices[i], 1);
        }
    }
}
function resetGameObjects(state, svg) {
    state.asteroids.forEach((asteroid) => {
        svg.removeChild(asteroid.elem.elem);
    });
    state.bullets.forEach((bullet) => {
        svg.removeChild(bullet.elem.elem);
    });
    state.asteroids = [];
    state.bullets = [];
    if (state.ship) {
        const shipPolygon = state.ship.elem.elem.firstElementChild;
        if (shipPolygon) {
            shipPolygon.setAttribute('style', `fill: ${playerColourPalette[5 - state.lives]}; stroke: #1976D2; stroke-width: 2)`);
        }
    }
}
if (typeof window != 'undefined')
    window.onload = () => {
        asteroids();
    };
//# sourceMappingURL=asteroids.js.map